# Aerodynamic modelling



## General workflow

This is intended to be used in combination with the sail making FreeCAD macros in the sister project. So it is intended that a rough wing shape be generated in FreeCAD and the lists of numbers for chords, spans, dihedrals etc be cut and pasted from the Configurations spreadsheet into the Wing Gen iPython notebook. This notebook is then used to generate an OpenVSP file. This file can then be used for analysis, such as the Vortex Lattice Model results shown below. If it is edited in OpenVSP then it should be saved and the Wing Gen notebook can be used to read this file to create the lists which can then be pasted back into the FreeCAD document.

![Screenshot.png](/examples/Screenshot.png)

## To do


- Interpolate between multiple wing sections
- Create region of single surface wing section at trailing edge

## Dependencies


- FreeCAD and sail-making macros
- OpenVSP with the API installed
- Jupyter Lab or Notebook with the VSP kernel accessible
